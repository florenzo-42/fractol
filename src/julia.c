/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 17:57:53 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 17:57:53 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		julia(const t_data *data, int x0, int y0)
{
	double		x;
	double		y;
	int			i;
	double		tmp;

	x = scale_x(data, x0);
	y = scale_y(data, y0);
	i = 0;
	while (x * x + y * y < 4 && i < data->max_iteration)
	{
		tmp = x * x - y * y;
		y = 2 * x * y + data->yparam;
		x = tmp + data->xparam;
		i += 1;
	}
	return (i);
}
