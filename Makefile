CC = clang
CFLAGS = -Wall -Wextra  -Werror -Iminilibx_macos -Ilibft -Iinc

NAME = fractol
LIBFT = libft/libft.a
MLX = minilibx_macos/libmlx.a
SRC = main.c \
	  mandelbrot.c \
	  hooks.c \
	  plot_utils.c \
	  colors.c \
	  julia.c \
	  burning_ship.c \
	  zoom.c \
	  onkeypress.c \
	  threads.c
SRC := $(addprefix src/, $(SRC))
OBJ = $(SRC:.c=.o)

all: $(NAME)
$(NAME): $(LIBFT) $(MLX) $(OBJ)
	$(CC) $(CFLAGS) $(LIBFT) $(MLX) $(OBJ) -o $(NAME) -framework OpenGL -framework Appkit

$(OBJ): inc/fractol.h

$(LIBFT):
	make -C libft

$(MLX):
	make -C minilibx_macos

clean:
	make -C libft clean
	make -C minilibx_macos clean
	rm -f $(OBJ)

fclean: clean
	make -C libft fclean
	rm -f $(NAME)

re: fclean all

.PHONY: fclean clean re
