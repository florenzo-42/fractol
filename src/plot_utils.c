/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   plot_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 18:12:33 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 18:12:33 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static inline void	put_px_in_img(const t_data *data, const int x,
		const int y, const int color)
{
	data->img[y * WINDOW_SIZE_X + x] = color;
}

void				*fill_image(void *ptr)
{
	int					x;
	int					y;
	const t_thread_data	*td = ptr;

	y = td->thread_id;
	while (y < WINDOW_SIZE_Y)
	{
		x = 0;
		while (x < WINDOW_SIZE_X)
		{
			put_px_in_img(td->data, x, y,
					(*td->data->color)(td->data,
						(*td->data->fractal)(td->data, x, y)));
			x += 1;
		}
		y += THREAD_NUMBER;
	}
	pthread_exit(NULL);
}

double				scale_x(const t_data *data, const int x)
{
	return ((((double)x * (data->low_x - data->high_x))
				/ (double)WINDOW_SIZE_X) + data->high_x);
}

double				scale_y(const t_data *data, const int y)
{
	return ((((double)y * (data->low_y - data->high_y))
				/ (double)WINDOW_SIZE_Y) + data->high_y);
}
