/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   burning_ship.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 17:52:05 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 17:52:05 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		burning_ship(const t_data *data, int x0, int y0)
{
	t_abxy		k;
	int			i;
	double		tmp;

	k.x = scale_x(data, x0);
	k.y = scale_y(data, y0);
	k.a = k.x;
	k.b = k.y;
	i = 0;
	while (k.a * k.a + k.b * k.b < 4.0 && i < data->max_iteration)
	{
		tmp = k.a * k.a - k.b * k.b + k.x;
		k.b = fabs(data->xparam * k.a * k.b) + k.y;
		k.a = fabs(tmp);
		i += 1;
	}
	return (i);
}
