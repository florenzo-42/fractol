# Fractol
A 42 project to discover graphics and generate fancy fractals.
### Features
Run `./fractol` without arguments for controls
* Multiple fractals to choose from, [Mandelbrot](https://en.wikipedia.org/wiki/Mandelbrot_set), [Julia](https://en.wikipedia.org/wiki/Julia_set), [Burning ship](https://en.wikipedia.org/wiki/Burning_Ship_fractal), etc
* Some fractals change their shape according to your mouse position (can be paused to take screenshots)
* Multiple color themes
* Click anywhere to center image and zoom with the mouse wheel
* Multithreaded, 8 threads by default, change that in [the header](inc/fractol.h)
### Screenshots
* Julia  
![julia](screenshots/julia.png)  
* Tricorn  
![tricorn](screenshots/tricorn.png)  
NOTE:  
To make this last one appear make mandelbrot interactive (press space) and move your mouse to the left  
* Mandelbrot  
![mandelbrot](screenshots/mandelbrot.png)  
### Build
Unfortunately this project has to be done with the minilibx, an educational graphics library that only runs on macOS.
If you still want to try it out :
```
git clone --recursive http://gitlab.com/florenzo-42/fractol
cd fractol
make
./fractol mandelbrot
```