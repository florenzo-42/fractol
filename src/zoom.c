/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zoom.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 18:21:19 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 18:21:19 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		onmouseclick(int button, int x, int y, t_data *data)
{
	double	offset;

	if (button == SCROLL_UP || button == SCROLL_DOWN)
	{
		offset = (data->high_x - data->low_x) * ZOOM_FACTOR;
		offset *= button == SCROLL_UP ? 1 : -1;
		data->low_x += offset;
		data->high_x -= offset;
		data->low_y += offset;
		data->high_y -= offset;
		update_window(data);
	}
	else if (button == 1)
	{
		offset = (((double)x / (double)WINDOW_SIZE_X) - 0.5) *
			(data->high_x - data->low_x);
		data->low_x -= offset;
		data->high_x -= offset;
		offset = (((double)y / (double)WINDOW_SIZE_Y) - 0.5) *
			(data->high_y - data->low_y);
		data->low_y -= offset;
		data->high_y -= offset;
		update_window(data);
	}
	return (0);
}
