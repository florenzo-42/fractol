/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 18:03:24 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 18:03:24 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int				exit_hook(t_data *data)
{
	(void)data;
	exit(EXIT_SUCCESS);
}

void			change_fractal_function(t_data *data, int i)
{
	data->fractal_index += i;
	data->fractal_index += sizeof(g_fractals) / sizeof(g_fractals[0]);
	data->fractal_index %= sizeof(g_fractals) / sizeof(g_fractals[0]);
	data->fractal = g_fractals[data->fractal_index];
}

void			change_color_function(t_data *data, int i)
{
	data->color_index += i;
	data->color_index += sizeof(g_colors) / sizeof(g_colors[0]);
	data->color_index %= sizeof(g_colors) / sizeof(g_colors[0]);
	data->color = g_colors[data->color_index];
}

int				onmousemove(int x, int y, t_data *data)
{
	if (data->interactive
			&& (data->fractal == &mandelbrot
				|| data->fractal == &julia
				|| data->fractal == &angel
				|| data->fractal == &burning_ship))
	{
		data->xparam = (x - WINDOW_SIZE_X / 2) * 0.01;
		data->yparam = (y - WINDOW_SIZE_Y / 2) * 0.01;
		update_window(data);
	}
	return (0);
}
