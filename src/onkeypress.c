/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   onkeypress.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 18:02:16 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 18:02:16 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int	is_x_in_array(const int x, const int *array, const size_t len)
{
	size_t		i;

	i = 0;
	while (i < len)
	{
		if (array[i] == x)
			return (TRUE);
		i += 1;
	}
	return (FALSE);
}

static void	onkeypress2(const int keycode, t_data *data)
{
	if (keycode == 45)
		change_color_function(data, 1);
	else if (keycode == 35)
		change_color_function(data, -1);
	else if (keycode == 49)
		data->interactive = !data->interactive;
	else if (keycode == 38)
		data->max_iteration /= data->max_iteration == 2 ? 1 : 2;
	else if (keycode == 40)
		data->max_iteration *= 2;
	else if (keycode == 29)
	{
		data->low_x = -2.0;
		data->low_y = -2.0;
		data->high_x = 2.0;
		data->high_y = 2.0;
	}
}

int			onkeypress(int keycode, t_data *data)
{
	static int	changing[] = {3, 11, 29, 38, 40, 45, 35};

	if (keycode == 53 || keycode == 12)
		exit(EXIT_SUCCESS);
	if (keycode == 3)
		change_fractal_function(data, 1);
	else if (keycode == 11)
		change_fractal_function(data, -1);
	else if (keycode == 20)
		data->fractal = &burning_ship;
	else if (keycode == 27)
		onmouseclick(SCROLL_DOWN, 0, 0, data);
	else if (keycode == 24)
		onmouseclick(SCROLL_UP, 0, 0, data);
	else
		onkeypress2(keycode, data);
	if (is_x_in_array(keycode, changing,
				sizeof(changing) / sizeof(changing[0])))
		update_window(data);
	return (0);
}
