/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 18:21:12 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 18:21:12 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	usage(void)
{
	ft_printf("./fractol [fractal]\n"
			"Where fractal is a string in the following list :\n"
			"[\"mandelbrot\", \"julia\", \"burning_ship\", \"angel\", "
			"\"mandelbrot3\", \"mandelbrot5\", \"mandelbrotm1\"]\n"
			"CONTROLS:\n"
			"f/b : cycle forward / backward in the fractal list\n"
			"0 : center map and reset zoom\n"
			"n/p : next/previous color scheme\n"
			"j/k : halve/double the iterations (don't abuse of k)\n"
			"left-click : center map\n"
			"mouse wheel or +/- : zoom/unzoom\n"
			"space : toggle interactivity (try this on julia)\n"
			"q/ESC : quit\n"
);
	exit(EXIT_FAILURE);
}

void	mlx_sucks_hard(void)
{
	ft_printf("mlx sucks so hard it failed to start a fucking window "
			"and create an image omg\n");
	exit(EXIT_FAILURE);
}

void	parse_args(const int ac, char **av, t_data *data)
{
	int			i;
	const char	*list[7] = {
		"mandelbrot",
		"julia",
		"burning_ship",
		"angel",
		"mandelbrot3",
		"mandelbrot5",
		"mandelbrotm1"
	};

	if (ac != 2)
		usage();
	i = 0;
	data->fractal_index = -1;
	while (i < 7)
	{
		if (ft_strcmp(list[i], av[1]) == 0)
			data->fractal_index = i;
		i += 1;
	}
	if (data->fractal_index == -1)
		usage();
}

void	init_data(t_data *data)
{
	if ((data->mlx = mlx_init()) == NULL
			|| (data->win = mlx_new_window(data->mlx, WINDOW_SIZE_X,
					WINDOW_SIZE_Y, WINDOW_TITLE)) == NULL
			|| (data->img_ptr = mlx_new_image(data->mlx, WINDOW_SIZE_X,
					WINDOW_SIZE_Y)) == NULL
			|| (data->img = (int*)mlx_get_data_addr(data->img_ptr,
					&data->bits_per_pixel, &data->size_line, &data->endian))
			== NULL)
		mlx_sucks_hard();
	data->fractal = g_fractals[data->fractal_index];
	data->max_iteration = 16;
	data->color = &black_and_white;
	data->color_index = 0;
	data->low_x = -2.0;
	data->low_y = -2.0;
	data->high_x = 2.0;
	data->high_y = 2.0;
	data->xparam = 2.0;
	data->yparam = 2.0;
	data->interactive = FALSE;
}

int		main(int ac, char **av)
{
	t_data	data;

	parse_args(ac, av, &data);
	init_data(&data);
	onkeypress(40, &data);
	mlx_mouse_hook(data.win, &onmouseclick, &data);
	mlx_key_hook(data.win, &onkeypress, &data);
	mlx_hook(data.win, 17, 0, &exit_hook, NULL);
	mlx_hook(data.win, 6, 0, &onmousemove, &data);
	mlx_loop(data.mlx);
}

int		(*const g_colors[5])(t_data*, int) = {
	&black_and_white,
	&white_and_black,
	&squared,
	&rgbmod,
	&test
};

int		(*const g_fractals[7])(const t_data*, int, int) = {
	&mandelbrot,
	&julia,
	&burning_ship,
	&angel,
	&mandelbrot3,
	&mandelbrot5,
	&mandelbrotm1
};

