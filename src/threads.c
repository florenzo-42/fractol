/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   threads.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 18:03:22 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 18:03:22 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_thread_data	*thread_data_array(t_data *data)
{
	t_thread_data	*new;
	int				i;

	new = ealloc(sizeof(t_thread_data) * THREAD_NUMBER);
	i = 0;
	while (i < THREAD_NUMBER)
	{
		new[i].thread_id = i;
		new[i].data = data;
		i += 1;
	}
	return (new);
}

void			thread_error(void)
{
	ft_printf("a thread error occured\n");
	exit(EXIT_FAILURE);
}

void			update_window(t_data *data)
{
	static pthread_t		threads[THREAD_NUMBER];
	static t_thread_data	*thread_datas = NULL;
	int						i;

	if (thread_datas == NULL)
		thread_datas = thread_data_array(data);
	i = 0;
	while (i < THREAD_NUMBER)
	{
		if (pthread_create(threads + i, NULL, &fill_image, thread_datas + i))
			thread_error();
		i += 1;
	}
	i = 0;
	while (i < THREAD_NUMBER)
	{
		if (pthread_join(threads[i], NULL))
			thread_error();
		i += 1;
	}
	mlx_clear_window(data->mlx, data->win);
	mlx_put_image_to_window(data->mlx, data->win, data->img_ptr, 0, 0);
}
