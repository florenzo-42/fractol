/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 17:59:00 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 17:59:00 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		angel(const t_data *data, int x0, int y0)
{
	t_abxy		k;
	int			i;
	double		tmp;

	k.x = scale_x(data, x0);
	k.y = scale_y(data, y0);
	k.a = k.x;
	k.b = k.y;
	i = 0;
	while (k.a * k.a + k.b * k.b < 4.0 && i < data->max_iteration)
	{
		tmp = k.a * k.a - k.b * k.b - k.x;
		k.b = data->xparam * k.a * k.b + k.y;
		k.a = tmp;
		i += 1;
	}
	return (i);
}

int		mandelbrot(const t_data *data, int x0, int y0)
{
	t_abxy		k;
	int			i;
	double		tmp;

	k.x = scale_x(data, x0);
	k.y = scale_y(data, y0);
	k.a = 0.0;
	k.b = 0.0;
	i = 0;
	while (k.a * k.a + k.b * k.b < 4.0 && i < data->max_iteration)
	{
		tmp = k.a * k.a - k.b * k.b + k.x;
		k.b = data->xparam * k.a * k.b + k.y;
		k.a = tmp;
		i += 1;
	}
	return (i);
}

int		mandelbrot3(const t_data *data, int x0, int y0)
{
	t_abxy		k;
	int			i;
	double		tmp;

	k.x = scale_x(data, x0);
	k.y = scale_y(data, y0);
	k.a = 0.0;
	k.b = 0.0;
	i = 0;
	while (k.a * k.a + k.b * k.b < 4.0 && i < data->max_iteration)
	{
		tmp = pow(k.a, 3) - 3 * k.a * pow(k.b, 2) + k.x;
		k.b = 3 * pow(k.a, 2) * k.b - pow(k.b, 3) + k.y;
		k.a = tmp;
		i += 1;
	}
	return (i);
}

int		mandelbrot5(const t_data *data, int x0, int y0)
{
	t_abxy		k;
	int			i;
	double		tmp;

	k.x = scale_x(data, x0);
	k.y = scale_y(data, y0);
	k.a = 0.0;
	k.b = 0.0;
	i = 0;
	while (k.a * k.a + k.b * k.b < 4.0 && i < data->max_iteration)
	{
		tmp = pow(k.a, 5) - 10 * pow(k.a, 3) * pow(k.b, 2)
			+ 5 * k.a * pow(k.b, 4) + k.x;
		k.b = 5 * pow(k.a, 4) * k.b - 10 * pow(k.a, 2) * pow(k.b, 3)
			+ pow(k.b, 5) + k.y;
		k.a = tmp;
		i += 1;
	}
	return (i);
}

int		mandelbrotm1(const t_data *data, int x0, int y0)
{
	t_abxy		k;
	int			i;
	double		tmp;

	k.x = scale_x(data, x0);
	k.y = scale_y(data, y0);
	k.a = k.x;
	k.b = k.y;
	i = 0;
	while ((tmp = (k.a * k.a + k.b * k.b)) < 4.0 && i < data->max_iteration)
	{
		if (tmp == 0)
			break ;
		k.a = k.a / tmp + k.x;
		k.b = -k.b / tmp + k.y;
		i += 1;
	}
	return (i);
}
