/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 17:57:59 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 17:57:59 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		black_and_white(t_data *data, int iter)
{
	iter = 255 * iter / data->max_iteration;
	return ((iter << 16) + (iter << 8) + iter);
}

int		white_and_black(t_data *data, int iter)
{
	return (0xffffff - black_and_white(data, iter));
}

int		rgbmod(t_data *data, int iter)
{
	(void)data;
	if (iter % 3 == 2)
		return (iter % 256 << 16);
	else if (iter % 3 == 1)
		return (iter % 256 << 8);
	else
		return (iter % 256);
}

int		squared(t_data *data, int iter)
{
	(void)data;
	return (iter * iter);
}

int		test(t_data *data, int iter)
{
	(void)data;
	return (((iter % 16) * 16 << 16) + ((iter % 8) * 32 << 8)
			+ (iter % 4) * 64);
}
