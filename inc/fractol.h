/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 18:21:43 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/31 18:21:43 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include <math.h>
# include <pthread.h>
# include "libft.h"
# include "mlx.h"
# define WINDOW_SIZE_X 1024
# define WINDOW_SIZE_Y 1024
# define WINDOW_TITLE "fractol"
# define SCROLL_UP 4
# define SCROLL_DOWN 5
# define ZOOM_FACTOR 0.1
# define THREAD_NUMBER 8

typedef struct	s_data
{
	void		*mlx;
	void		*win;
	void		*img_ptr;
	int			*img;
	int			(*fractal)(const struct s_data*, int, int);
	int			(*color)(struct s_data*, int);
	int			color_index;
	int			fractal_index;
	double		low_x;
	double		high_x;
	double		low_y;
	double		high_y;
	double		xparam;
	double		yparam;
	int			bits_per_pixel;
	int			size_line;
	int			endian;
	int			max_iteration;
	int			interactive;
}				t_data;

extern int		(*const g_colors[5])(t_data*, int);
extern int		(*const g_fractals[7])(const t_data*, int, int);

typedef struct	s_thread_data
{
	int			thread_id;
	t_data		*data;
}				t_thread_data;

typedef struct	s_abxy
{
	double		a;
	double		b;
	double		x;
	double		y;
}				t_abxy;

/*
** fractals
*/

int				burning_ship(const t_data *data, int x0, int y0);
int				julia(const t_data *data, int x0, int y0);
int				mandelbrot(const t_data *data, int x0, int y0);
int				angel(const t_data *data, int x0, int y0);
int				mandelbrot3(const t_data *data, int x0, int y0);
int				mandelbrot5(const t_data *data, int x0, int y0);
int				mandelbrotm1(const t_data *data, int x0, int y0);

/*
** colorschemes
*/

int				black_and_white(t_data *data, int iter);
int				white_and_black(t_data *data, int iter);
int				squared(t_data *data, int iter);
int				rgbmod(t_data *data, int iter);
int				test(t_data *data, int iter);

void			update_window(t_data *data);
int				exit_hook(t_data *data);
void			*fill_image(void *ptr);
int				onkeypress(int keycode, t_data *data);
int				onmousemove(int x, int y, t_data *data);
double			scale_y(const t_data *data, const int y);
double			scale_x(const t_data *data, const int x);
int				onmouseclick(int button, int x, int y, t_data *data);
void			change_color_function(t_data *data, int i);
void			change_fractal_function(t_data *data, int i);

#endif
